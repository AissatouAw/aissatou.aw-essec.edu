#This program print Hello
print ("Hello")



#This program print Hello my name is Aïssatou
print ("Hello my name is Aïssatou")



#This program print I'm a beginner at Python
print ("I'm a beginner at Python")




# This program is giving us the actual date

from datetime import date

today = date.today()
print("La date du jour = ", today)



# This program adds two numbers (5,5 and 4,4)

num1 = 5.5
num2 = 4.4

# Add two numbers
sum = num1 + num2

# Display the sum
print('La somme de {0} et {1} est {2}'.format(num1, num2, sum))



# Voir si un nombre est pair ou impair

number = 29384

if(number % 2 == 0):
    print("{0} est un nombre pair".format(number))
else:
    print("{0} est un nombre impair".format(number))

    

# Generate a number between 0 and 99

# importing the random module
print('Nous allons générer un nombre au hasard entre 0 et 99:')
import random

print(random.randint(0,99))



# Python Program to Print Odd Numbers from 1 to 199

print('Voici la liste des nombres impairs entre 21 et 45 compris:')
maximum = 45

for number in range(21, maximum + 1):
    if(number % 2 != 0):
        print("{0}".format(number))



#This program convert temperature from celsius to fahrenheit

# change this value for a different result
celsius = 37.5

# calculate fahrenheit
fahrenheit = (celsius * 1.8) + 32
print('%0.1f degré Celsius est egal a %0.1f degree Fahrenheit' %(celsius,fahrenheit))



# This program calculate the area of triangle (5,6 and 7cm)

a = 5
b = 6
c = 7

# calculate the semi-perimeter
s = (a + b + c) / 2

# calculate the area
area = (s*(s-a)*(s-b)*(s-c)) ** 0.5
print('Air dun triangle avec des cotes 5, 6 and 7 cm: %0.2f' %area)




# This program calculate the square root of 25

num = 25 

num_sqrt = num ** 0.5
print('La racine carrée de %0.3f is %0.3f'%(num ,num_sqrt))




#This program calculate the BMI ou IMC en français
print('Calcul de l IMC pour une femme de 1m70 et 80kg')
Taille=170
Poids=70
Taille = Taille/100
IMC=Poids/(Taille*Taille)
print("IMC de: ",IMC)
if(IMC>0):
	if(IMC<=16):
		print("Tu es en extreme maigreur")
	elif(IMC<=18.5):
		print("Tu es en sous-poids")
	elif(IMC<=25):
		print("Tu es en bonne santé")
	elif(IMC<=30):
		print("Tu es en surpoids")
	else: print("Tu es obèse")
else:("Verifier les details")



# This program convert numeral numbers into roman numbers
def intToRoman(num):

	# Dictionnaire
	# when placed at different places
	m = ["", "M", "MM", "MMM"]
	c = ["", "C", "CC", "CCC", "CD", "D",
		"DC", "DCC", "DCCC", "CM "]
	x = ["", "X", "XX", "XXX", "XL", "L",
		"LX", "LXX", "LXXX", "XC"]
	i = ["", "I", "II", "III", "IV", "V",
		"VI", "VII", "VIII", "IX"]

	# Conversion en chiffre romains
	thousands = m[num // 1000]
	hundreds = c[(num % 1000) // 100]
	tens = x[(num % 100) // 10]
	ones = i[num % 10]

	ans = (thousands + hundreds +
		tens + ones)

	return ans

# Calcul
if __name__ == "__main__":
	number = 1999
	print('Voici mon année de naissance 1999 en chiffre romain:')
	print(intToRoman(number))




# Ce programme nous affiche les tables de multiplication de 1 à 10

print('Voici les tables de multiplication de 1 à 10:')
for i in range(1, 11):
    for j in range(1, 11):
        print(i * j, end=" ")
    print("\t\t")



#This program plays automatically Tic Tac Toe ou "Jeu du Morpion"

print('Voici le jeu du Morpion automatique avec 2 joueurs')
import numpy as np 
import random 
from time import sleep

def create_board(): 
    return(np.array([[0, 0, 0], 
                     [0, 0, 0], 
                     [0, 0, 0]]))


def possibilities(board): 
    l = [] 
      
    for i in range(len(board)): 
        for j in range(len(board)): 
              
            if board[i][j] == 0: 
                l.append((i, j)) 
    return(l)


def random_place(board, player): 
    selection = possibilities(board) 
    current_loc = random.choice(selection) 
    board[current_loc] = player 
    return(board) 
  
def row_win(board, player): 
    for x in range(len(board)): 
        win = True
          
        for y in range(len(board)): 
            if board[x, y] != player: 
                win = False
                continue
                  
        if win == True: 
            return(win) 
    return(win) 
  
def col_win(board, player): 
    for x in range(len(board)): 
        win = True
          
        for y in range(len(board)): 
            if board[y][x] != player: 
                win = False
                continue
                  
        if win == True: 
            return(win) 
    return(win) 
  
def diag_win(board, player): 
    win = True
    y = 0
    for x in range(len(board)): 
        if board[x, x] != player: 
            win = False
    if win: 
        return win 
    win = True
    if win: 
        for x in range(len(board)): 
            y = len(board) - 1 - x 
            if board[x, y] != player: 
                win = False
    return win 
  
def evaluate(board): 
    winner = 0
      
    for player in [1, 2]: 
        if (row_win(board, player) or
            col_win(board,player) or 
            diag_win(board,player)): 
                 
            winner = player 
              
    if np.all(board != 0) and winner == 0: 
        winner = -1
    return winner 
  
def play_game(): 
    board, winner, counter = create_board(), 0, 1
    print(board) 
    sleep(2) 
      
    while winner == 0: 
        for player in [1, 2]: 
            board = random_place(board, player) 
            print("Plateau de jeu après " + str(counter) + " tours") 
            print(board) 
            sleep(2) 
            counter += 1
            winner = evaluate(board) 
            if winner != 0: 
                break
    return(winner) 
  
print("Le gagnant est le joueur numéro: " + str(play_game()))



